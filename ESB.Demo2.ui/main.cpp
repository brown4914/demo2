//Demo 2
//Eric Brown
#include <iostream>
#include <conio.h>
#include <string>
using namespace std;

// Function prototype
void PrintSomething(); //Tells complier to look elsewhere for the function. 
int AddInts(int n1, int n2);
int AddInts(int n1, int n2, int &n3);

int Fact(int num)
{
	if (num == 0)
	{
		return 1;
	}

	return num * Fact(num - 1);
}

bool Confirm(string msg)
{
	char input = 'n';
	cout << msg;
	cin >> input; 
	if (input == 'y' || input == 'Y') return true;
	if (input == 'n' || input == 'N') return false;
	return Confirm(msg);

}

int main()
{
	//cout << "Hi"; // This will crash the app (stack overflow), recusrivse loop calling a function within itself
	//main(); // You can wind back the stack. 

	char quit = 'n';

	do
	{
			cout << Fact(5) << endl;
			
	} 
	while (Confirm("Would you like to continue? (y/n):"));

	PrintSomething();

	int i = 4;
	int j = 2; 
	int k = 36;

	cout << AddInts(i, j, k);



	_getch();
	return 0; 
}
void PrintSomething()
{
	cout << "Somthing" <<endl;
}

int AddInts(int n1, int n2)
{
	return n1 + n2;
}
// overload the add ints function
int AddInts(int n1, int n2, int &n3)//adding the & allows it to change the variable k. reference variable 
{
	n1++;
	n2++;
	n3++;

	return n1 + n2 + n3;
}